import requests
from requests.auth import HTTPBasicAuth

from django.conf import settings


class AuthClient:

    def __init__(self, base_url):
        self.base_url = base_url
        self.auth = HTTPBasicAuth(settings.APARTX_LOGIN, settings.APARTX_PASSWORD)

    def register(self, email: str, password: str):
        body = {
            'username': email,
            'password': password
        }
        requests.post(self.base_url + '/register/', json=body, auth=self.auth)
