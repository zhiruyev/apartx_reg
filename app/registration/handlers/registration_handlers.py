from random import randint

from django.core.mail import send_mail
from django.utils.translation import gettext as _
from django.conf import settings
from rest_framework import exceptions

from app.libs.clients.auth_client import AuthClient
from app.registration.constants import RegistrationStatuses
from app.registration.entities.registration_entities import (
    RegistrationInitInputEntity,
    RegistrationInitOutputEntity,
    RegistrationConfirmEmailInputEntity,
    RegistrationFinishInputEntity,
)
from app.registration.models import (
    RegistrationApplication,
    RegistrationApplicationCode,
)

auth_client: AuthClient = settings.AUTH_CLIENT


class RegistrationApplicationHandler:

    def init(self, input_entity: RegistrationInitInputEntity) -> RegistrationInitOutputEntity:
        finished_application = RegistrationApplication.objects.filter(
            email=input_entity.email,
            status=RegistrationStatuses.FINISHED.value
        )
        if finished_application.exists():
            raise exceptions.PermissionDenied(_("Registration already finished"))

        email_confimed_application = RegistrationApplication.objects.filter(
            email=input_entity.email,
            status=RegistrationStatuses.EMAIL_CONFIRMED.value
        )
        if email_confimed_application.exists():
            application = email_confimed_application.first()
            output_entity = RegistrationInitOutputEntity(
                uuid=application.uuid,
                status=application.status
            )

            return output_entity

        application = RegistrationApplication.objects.create(
            email=input_entity.email,
        )

        confirmation_code = randint(111111, 999999)
        RegistrationApplicationCode.objects.create(
            application=application,
            code=confirmation_code,
        )

        send_mail(
            'Код подтверждения ApartX cleaning',
            f'Ваш код подтверждения: {confirmation_code}',
            settings.EMAIL_HOST_USER,
            [application.email],
            fail_silently=True
        )

        output_entity = RegistrationInitOutputEntity(
            uuid=application.uuid,
            status=application.status
        )

        return output_entity

    def confirm_email(
        self,
        input_entity: RegistrationConfirmEmailInputEntity,
        application: RegistrationApplication,
    ) -> None:
        last_app_code: RegistrationApplicationCode = RegistrationApplicationCode.objects.filter(
            application=application
        ).last()
        if last_app_code.code == input_entity.code:
            application.email_confirmed()
        else:
            raise exceptions.PermissionDenied(_("Code is incorrect"))

    def finish(
        self,
        input_entity: RegistrationFinishInputEntity,
        application: RegistrationApplication,
    ) -> None:
        if application.status == RegistrationStatuses.EMAIL_CONFIRMED.value:
            auth_client.register(
                email=application.email,
                password=input_entity.password
            )
            application.finish()
        else:
            raise exceptions.PermissionDenied(_("Confirm email first"))
