from rest_framework.routers import DefaultRouter
from app.registration.views import registration_views

app_name = "registration"

router = DefaultRouter()
router.register(
    "registration",
    registration_views.RegistrationApplicationViewSet,
    basename="registration"
)

urlpatterns = router.get_urls()
