from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from app.registration.handlers.registration_handlers import RegistrationApplicationHandler
from app.registration.models import RegistrationApplication
from app.registration.serializers.registration_serializers import (
    RegistrationInitInputSerializer,
    RegistrationInitOutputSerializer,
    RegistrationConfirmEmailInputSerializer,
    RegistrationFinishInputSerializer,
)


class RegistrationApplicationViewSet(
    GenericViewSet,
):
    queryset = RegistrationApplication.objects.all()

    @action(methods=['post'], detail=False)
    def init(self, request, *args, **kwargs):
        serializer = RegistrationInitInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        entity = RegistrationApplicationHandler().init(input_entity=input_entity)

        return Response(RegistrationInitOutputSerializer(entity).data)

    @action(methods=['post'], detail=True)
    def confirm_email(self, request, *args, **kwargs):
        serializer = RegistrationConfirmEmailInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        app = self.get_object()

        RegistrationApplicationHandler().confirm_email(
            application=app,
            input_entity=input_entity,
        )

        return Response()

    @action(methods=['post'], detail=True)
    def finish(self, request, *args, **kwargs):
        serializer = RegistrationFinishInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        app = self.get_object()

        RegistrationApplicationHandler().finish(
            application=app,
            input_entity=input_entity,
        )

        return Response()
