from uuid import uuid4

from dataclasses import dataclass


@dataclass
class RegistrationInitInputEntity:
    email: str


@dataclass
class RegistrationInitOutputEntity:
    uuid: uuid4
    status: int


@dataclass
class RegistrationConfirmEmailInputEntity:
    code: int


@dataclass
class RegistrationFinishInputEntity:
    password: str
